#include<systemc.h>
#include "test_bench.hpp"
 
int sc_main(int argc, char* argv[]) {
    ram ram1("ram");
    sc_set_time_resolution(1, SC_PS);
 
    sc_trace_file *wf = sc_create_vcd_trace_file("data");
    sc_trace(wf, ram1.addr, "addr");
    sc_trace(wf, ram1.data, "data");
    sc_trace(wf, ram1.enable, "enable");
    sc_trace(wf, ram1.readwr, "readwr");
 
    sc_signal   enable, readwr;
    sc_signal > data;
    sc_signal<sc_lv > addr;
 
    ram1.rw(rw);
    ram1.data(data);
    ram1.addr(addr);
    ram1.enable(enable);
 
    enable  = 0;
    data    = 0;
    addr = 0;
    readwr      = 0;
    sc_start(0, SC_PS);
 
    readwr      = 1;
    addr.write(0x10);
    data.write(0x110011);
    enable  = 1;
    sc_start(5, SC_PS);
 
    enable  = 0;
    sc_start(5, SC_PS);
 
    addr.write(0x12);
    data.write(0x1100);
    enable  = 1;
    sc_start(5, SC_PS);
 
    addr.write(0x8);
    data.write(0x1010);
    sc_start(5, SC_PS);
 
    readwr      = 0;
    addr = 0x10;
    sc_start(5, SC_PS);
 
    addr = 0x1;
    sc_start(5, SC_PS);
 
    addr = 0x3;
    sc_start(5, SC_PS);
 
    enable  = 0;
    sc_start(1, SC_PS);
 
    ram1.write_data();
    sc_close_vcd_trace_file(wf);
 
    return(0);
}
