// ram.h
#include <systemc.h>
#define RAM_H_
SC_MODULE(ram)
{
sc_in<sc_int<8> > addr;
sc_in<bool> enable;
sc_in<bool> readwr;
sc_inout_rv<16> data;
void read_data();
void write_data();
sc_lv<16> ram_data[256];
SC_CTOR(ram)
{
SC_METHOD(read_data);
sensitive << addr << enable << readwr;
SC_METHOD(write_data);
sensitive << addr << enable << readwr << data;
}
};
