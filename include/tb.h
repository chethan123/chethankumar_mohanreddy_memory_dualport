#include <systemc.h>
#include "ram.h"
SC_MODULE (tb)
{
sc_in<bool> enable;
sc_in<bool> readwr;
sc_in<sc_in<8>> addr;
sc_inout<sc_int<16>> data;

void read_data();
void write_data();

SC_CTOR(tb) {
SC_CTHREAD (read_data > enable());
SC_CTHREAD (write_data > enable());
}
};
